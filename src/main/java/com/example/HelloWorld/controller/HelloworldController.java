package com.example.HelloWorld.controller;

import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/Hello")
public class HelloworldController {

	//@GetMapping()
	//public String helloMethod() {
	//	return "Hello My World";
	//}

	/**@GetMapping("/hello/{id}")
	@ResponseBody
	public String gethelloById(@PathVariable String id) {
	    return "ID: " + id;
	}
	**/
	
	/**
	//path variable
	@GetMapping("/employee/{id}/{name}")
	public HelloworldController getEmployeeById (@PathVariable long id,
			@PathVariable String name) {
		return new HelloworldController();
		
	}
	**/
	
	
	
	//Path variable
	@GetMapping("/api/employees/{id}/{name}")
	@ResponseBody
	public String getEmployeesByName(@PathVariable String id,@PathVariable String name) {
	    return "ID: " + id + ", NAME: " + name;
	}
	
	
	
	//query param (http://localhost:8080/Hello/api/query?id=23)
	@GetMapping("/api/query")
	@ResponseBody
	public String getquery(@RequestParam(defaultValue = "test") String id ) 
	{
	    return "ID: " + id;
	}
	
	//Without entering any value(http://localhost:8080/Hello/api/else)
	@GetMapping("/api/else")
	@ResponseBody
	public String getElse(@RequestParam Optional<String> id){
	    return "ID: " + id.orElseGet(() -> "Value not provided");
	}
	
	
	
	
	
	/**
	//query param default value
	@GetMapping("/api/query")
	@ResponseBody
	public String getTest(@RequestParam(defaultValue = "test") String id) {
	    return "ID: " + id;
	}
	**/
}
